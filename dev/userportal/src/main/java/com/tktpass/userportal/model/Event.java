package com.tktpass.userportal.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Event {
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	private String name;
	
	@OneToOne
	private Address address;
	
	private long startTime;
	
	private long duration;
	
	@Enumerated(EnumType.STRING)
	private EventType type;
	
	@Enumerated(EnumType.STRING)
	private EventStatus status;
	
	public static Event createEvent(String name, EventType type, Address address, long startTime, long duration) {
		Event event = new Event();
		event.name = name;
		event.type = type;
		event.address = address;
		event.startTime = startTime;
		event.duration = duration;
		return event;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Address getAddress() {
		return address;
	}

	public long getStartTime() {
		return startTime;
	}

	public long getDuration() {
		return duration;
	}

	public EventType getType() {
		return type;
	}

	public EventStatus getStatus() {
		return status;
	}

	public void setStatus(EventStatus status) {
		this.status = status;
	}

}
