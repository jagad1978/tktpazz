package com.tktpass.userportal.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class UserLogin {
	
	@Id
	private String userId;
	
	private String password;
	
	@Enumerated(EnumType.STRING)
	private LoginAgent agent;
	
	@Enumerated(EnumType.STRING)
	private UserStatus status;
	
	@Enumerated(EnumType.STRING)
	private UserRole role;
	
	private long createdOn;
	
	private long resetOn;
	
	private long lastAccessedOn;
	
	private long noOfFailureAttempts;
	
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public UserLogin() {}
	
	public UserLogin(String userId) {
		this.userId = userId;
	}
	
	public static UserLogin signUp(String userId, String securedPassword) {
		UserLogin userLogin = new UserLogin(userId);
		userLogin.setPassword(securedPassword);
		userLogin.setAgent(LoginAgent.SELF);
		userLogin.setCreatedOn(new Date().getTime());
		userLogin.setStatus(UserStatus.NEW);
		userLogin.setToken(generateRandomId());
		userLogin.role = UserRole.USER;
		return userLogin;
	}
	
	private static String generateRandomId() {
		//FIXME provide better string
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LoginAgent getAgent() {
		return agent;
	}

	public void setAgent(LoginAgent agent) {
		this.agent = agent;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}

	public long getResetOn() {
		return resetOn;
	}

	public void setResetOn(long resetOn) {
		this.resetOn = resetOn;
	}

	public long getLastAccessedOn() {
		return lastAccessedOn;
	}

	public void setLastAccessedOn(long lastAccessedOn) {
		this.lastAccessedOn = lastAccessedOn;
	}

	public long getNoOfFailureAttempts() {
		return noOfFailureAttempts;
	}

	public void setNoOfFailureAttempts(long noOfFailureAttempts) {
		this.noOfFailureAttempts = noOfFailureAttempts;
	}

	public String getUserId() {
		return userId;
	}

	public UserRole getRole() {
		return role;
	}
	
}
