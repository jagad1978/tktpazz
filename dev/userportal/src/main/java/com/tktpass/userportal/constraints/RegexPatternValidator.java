package com.tktpass.userportal.constraints;

import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RegexPatternValidator<X extends Annotation> implements ConstraintValidator<X, String> {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Pattern pattern;

	public RegexPatternValidator(String regex) {
		this.pattern = Pattern.compile(regex);
		this.logger.info("Regex registered {}", regex);
	}

	@Override
	public void initialize(X constraintAnnotation) {
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean valid = value != null && pattern.matcher(value).matches();
		this.logger.info("{} found to be a {} value", value, valid ? "valid" : "invalid");
		return valid;
	}

}
