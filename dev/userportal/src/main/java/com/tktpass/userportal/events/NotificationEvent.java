package com.tktpass.userportal.events;

import org.springframework.context.ApplicationEvent;

import com.tktpass.userportal.model.NotificationType;
import com.tktpass.userportal.model.UserType;

public class NotificationEvent extends ApplicationEvent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private NotificationType type;

	private String message;

	private UserType processorType;
	
	private String processorId;

	public NotificationEvent(String sourceId, String message, NotificationType type, UserType processorType) {
		super(sourceId);
		this.type = type;
		this.message = message;
		this.processorType = processorType;
	}

	public NotificationType getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

	public UserType getProcessorType() {
		return processorType;
	}

	public String getProcessorId() {
		return processorId;
	}

	public void setProcessorId(String processorId) {
		this.processorId = processorId;
	}

}
