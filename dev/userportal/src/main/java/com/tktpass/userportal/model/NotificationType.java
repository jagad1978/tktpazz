package com.tktpass.userportal.model;

public enum NotificationType {
	
	EVENT_REQUEST, EVENT_REQUEST_REMINDER

}
