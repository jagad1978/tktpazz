package com.tktpass.userportal.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tktpass.userportal.model.UserProfile;

public interface UserProfileRepository extends CrudRepository<UserProfile, String> { 
	
	@Query("SELECT t FROM UserProfile t where t.emailId = :emailId")
	public UserProfile findByEmailId(@Param("emailId") String emailId);

}
