package com.tktpass.userportal.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Address {
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	private String name;
	
	private String addressLine1;
	
	private String addressLine2;
	
	private String city;
	
	private String state;
	
	private String country;
	
	private String mobile;
	
	private long zipcode;
	
	private String landmark;
	
	public Address() {}
	
	public Address(String name, String addressLine1, String addressLine2, String city, String state, String country, String mobile, long zipcode, String landmark) {
		this.name = name;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
		this.country = country;
		this.mobile = mobile;
		this.zipcode = zipcode;
		this.landmark = landmark;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getCountry() {
		return country;
	}

	public String getMobile() {
		return mobile;
	}

	public long getZipcode() {
		return zipcode;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setZipcode(long zipcode) {
		this.zipcode = zipcode;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

}
