package com.tktpass.userportal.resources.exceptions;

import org.springframework.http.HttpStatus;

public class EntityAlreadyExistsException extends BaseException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public EntityAlreadyExistsException(ErrMessage message) {
		super(message, HttpStatus.CONFLICT);
	}

}
