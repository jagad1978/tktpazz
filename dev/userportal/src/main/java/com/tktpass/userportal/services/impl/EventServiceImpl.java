package com.tktpass.userportal.services.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import com.tktpass.userportal.events.NotificationEvent;
import com.tktpass.userportal.model.Address;
import com.tktpass.userportal.model.Event;
import com.tktpass.userportal.model.EventRequest;
import com.tktpass.userportal.model.EventRequestStatus;
import com.tktpass.userportal.model.EventStatus;
import com.tktpass.userportal.model.EventType;
import com.tktpass.userportal.model.NotificationType;
import com.tktpass.userportal.model.UserType;
import com.tktpass.userportal.repositories.AddressRepository;
import com.tktpass.userportal.repositories.EventRepository;
import com.tktpass.userportal.repositories.EventRequestRepository;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.resources.exceptions.EntityNotFoundException;
import com.tktpass.userportal.resources.exceptions.ErrMessage;
import com.tktpass.userportal.services.EventService;
import com.tktpass.userportal.services.SessionInfo;
import com.tktpass.userportal.to.EventCreationTO;
import com.tktpass.userportal.to.EventRequestTO;
import com.tktpass.userportal.to.EventTO;

@Service
@Transactional
public class EventServiceImpl implements EventService {

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private EventRequestRepository eventRequestRepository;

	@Autowired
	private ApplicationEventPublisher eventPublisher;

	@Autowired
	private SessionInfo sessionInfo;

	@Override
	public EventRequestTO requestEvent(EventCreationTO eventCreationTO) throws BaseException {
		Address address = new AddressAdapter().adapt(eventCreationTO.getAddress());
		address = addressRepository.save(address);
		
		EventType eventType = EventType.valueOf(eventCreationTO.getType());

		Event event = Event.createEvent(eventCreationTO.getName(), eventType, address, eventCreationTO.getStartTime(), eventCreationTO.getDuration());
		event = eventRepository.save(event);

		EventRequest eventRequest = EventRequest.createEventRequest(event, sessionInfo.getUserId(),
				new Date().getTime());
		eventRequest = eventRequestRepository.save(eventRequest);

		eventPublisher.publishEvent(new NotificationEvent(eventRequest.getId(), "Event Requested",
				NotificationType.EVENT_REQUEST, UserType.ADMIN));

		return getEventRequestAdapter().adaptTo(eventRequest);
	}

	@Override
	public List<EventRequestTO> listEventRequests() throws BaseException {
		List<EventRequest> eventRequests = eventRequestRepository.listByCreator(sessionInfo.getUserId());
		return getEventRequestAdapter().adaptTo(eventRequests);
	}

	private Adapter<EventRequest, EventRequestTO> getEventRequestAdapter() {
		return new EventRequestAdapter();
	}

	private Adapter<Event, EventTO> getEventAdapter() {
		return new EventAdapter();
	}

	@Override
	public void sendReminder(String eventRequestId, String message) throws BaseException {
		EventRequest eventRequest = eventRequestRepository.findOne(eventRequestId);
		if (eventRequest == null) {
			throw new EntityNotFoundException(ErrMessage.EVENTREQUEST_NOT_FOUND);
		}
		eventPublisher.publishEvent(new NotificationEvent(eventRequest.getId(), message,
				NotificationType.EVENT_REQUEST_REMINDER, UserType.ADMIN));
	}
	
	@Override
	public void approve(String eventRequestId) throws BaseException {
		EventRequest eventRequest = eventRequestRepository.findOne(eventRequestId);
		if (eventRequest == null) {
			throw new EntityNotFoundException(ErrMessage.EVENTREQUEST_NOT_FOUND);
		}
		
		Event event = eventRequest.getEvent();
		event.setStatus(EventStatus.ACTIVE);
		eventRepository.save(event);
		
		eventRequest.setStatus(EventRequestStatus.APPROVED);
		eventRequestRepository.save(eventRequest);
	}

	@Override
	public List<EventTO> listFutureEvents(EventType... eventTypes) throws BaseException {
		List<Event> events = null;
		long currentTime = new Date().getTime();
		if (eventTypes == null || eventTypes.length == 0) {
			events = eventRepository.listFutureEvents(currentTime);
		} else if (eventTypes.length == 1) {
			events = eventRepository.listFutureEventsByType(eventTypes[0], currentTime);
		} else {
			events = eventRepository.listFutureEventsByTypes(Arrays.asList(eventTypes), currentTime);
		}
		return events != null ? getEventAdapter().adaptTo(events) : Collections.emptyList();
	}

	@Override
	public EventTO getEvent(String eventId) throws BaseException {
		Event event = eventRepository.findOne(eventId);
		if (event == null) {
			throw new BaseException(ErrMessage.EVENT_NOT_FOUND);
		}
		return getEventAdapter().adaptTo(event);
	}

}
