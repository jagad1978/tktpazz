package com.tktpass.userportal.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tktpass.userportal.model.Ticket;

public interface TicketRepository extends CrudRepository<Ticket, String>{

	@Query("SELECT t FROM Ticket t where t.listedBy = :userId and t.status = 'NEW'")
	public List<Ticket> listNewTicketsByUser(@Param("userId") String userId);
	
	@Query("SELECT t FROM Ticket t where t.status = 'NEW'")
	public List<Ticket> listNewTickets();
	
	@Query("SELECT t FROM Ticket t where t.eventId = :eventId and t.status = 'NEW'")
	public List<Ticket> listNewTicketsForEvent(@Param("eventId") String eventId);
	
	@Query("SELECT t FROM Ticket t where t.eventId in :eventIds and t.status = 'NEW'")
	public List<Ticket> listNewTicketsForEvents(@Param("eventIds") List<String> eventIds);

}
