package com.tktpass.userportal.services;

import java.util.List;

import com.tktpass.userportal.model.EventType;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.to.TicketCreationTO;
import com.tktpass.userportal.to.TicketTO;

public interface TicketService {

	public TicketTO createTicket(TicketCreationTO ticketCreationTo) throws BaseException;

	public List<TicketTO> listMyOpenTickets() throws BaseException;

	public List<TicketTO> listOpenTickets() throws BaseException;

	public List<TicketTO> listOpenTickets(EventType eventType) throws BaseException;

	public List<TicketTO> listOpenTickets(String eventId) throws BaseException;

	public List<TicketTO> listOpenTickets(String... eventIds) throws BaseException;

}
