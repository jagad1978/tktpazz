package com.tktpass.userportal.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Ticket {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private String parentId;

	private String eventId;

	private double value;

	private String description;
	
	private String listedBy;

	@Enumerated(EnumType.STRING)
	private TicketStatus status;

	@Enumerated(EnumType.STRING)
	private Currency currency;

	@ElementCollection
	private List<String> seats;

	public Ticket() {}

	public static Ticket createTicket(String eventId, String description, double value, Currency currency,
			List<String> seats, String listedBy) {
		Ticket ticket = new Ticket();
		ticket.eventId = eventId;
		ticket.description = description;
		ticket.value = value;
		ticket.currency = currency;
		if(seats != null && !seats.isEmpty()) {
			ticket.seats = new ArrayList<>(seats);
		}
		ticket.status = TicketStatus.NEW;
		ticket.listedBy = listedBy;
		return ticket;
	}

	public String getListedBy() {
		return listedBy;
	}

	public String getId() {
		return id;
	}

	public String getParentId() {
		return parentId;
	}

	public String getEventId() {
		return eventId;
	}

	public double getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public Currency getCurrency() {
		return currency;
	}

	public List<String> getSeats() {
		return seats == null ? Collections.emptyList() : Collections.unmodifiableList(seats);
	}

}
