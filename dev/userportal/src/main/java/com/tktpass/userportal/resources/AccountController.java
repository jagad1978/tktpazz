package com.tktpass.userportal.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tktpass.userportal.constraints.ValidPassword;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.to.AddressTO;
import com.tktpass.userportal.to.UserBasicInfoTO;
import com.tktpass.userportal.services.AccountService;

@RestController
@RequestMapping("/myaccount")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@PutMapping("/basicinfo")
	public ResponseEntity<UserBasicInfoTO> updateBasicDetails(@RequestParam String name,
			@RequestParam String mobileNumber) throws BaseException {
		UserBasicInfoTO basicInfoTO = accountService.updateBasicDetails(name, mobileNumber);
		return new ResponseEntity<UserBasicInfoTO>(basicInfoTO, HttpStatus.OK);
	}

	@GetMapping("/addresses")
	public ResponseEntity<List<AddressTO>> getAddresses() throws BaseException {
		List<AddressTO> addresses = accountService.getAddresses();
		return new ResponseEntity<List<AddressTO>>(addresses, HttpStatus.OK);
	}

	@GetMapping("/addresses/{id}")
	public ResponseEntity<AddressTO> getAddress(@PathVariable("id") String addressId) throws BaseException {
		AddressTO address = accountService.getAddress(addressId);
		return new ResponseEntity<AddressTO>(address, HttpStatus.OK);
	}

	@PostMapping("/addresses")
	public ResponseEntity<String> addAddress(@Valid AddressTO addressTO) throws BaseException {
		String addressId = accountService.addAddress(addressTO);
		return new ResponseEntity<String>(addressId, HttpStatus.CREATED);
	}

	@PutMapping("/addresses/{id}")
	public ResponseEntity<String> updateAddress(@PathVariable("id") String addressId, @Valid AddressTO addressTO)
			throws BaseException {
		boolean updated = accountService.updateAddress(addressId, addressTO);
		if (updated)
			return new ResponseEntity<String>("Address successfully updated", HttpStatus.OK);
		return new ResponseEntity<String>("Error while updating address", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@PutMapping("/password")
	public ResponseEntity<String> updatePassword(String oldPassword, @ValidPassword String newPassword) throws BaseException{
		boolean updated = accountService.updatePassword(oldPassword, newPassword);
		if(updated) {
			return new ResponseEntity<String>("Password successfully updated", HttpStatus.OK);
		}
		return new ResponseEntity<String>("Error while updating password", HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
