package com.tktpass.userportal.services;

import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.to.UserCreationTO;
import com.tktpass.userportal.to.UserInfoTO;
import com.tktpass.userportal.to.UserSignUpTO;

public interface UserService {

	public UserInfoTO getUserInfo(String userName) throws BaseException;

	public UserCreationTO signUp(UserSignUpTO userSignUpTO) throws BaseException;
	
	public boolean confirmRegistration(String userName, String token) throws BaseException;
	
	//FIXME change to boolean
	public String forgetPassword(String emailId) throws BaseException;

	public boolean resetPassword(String userId, String resetId, String newPassword) throws BaseException;
	
	public boolean updatePassword(String userId, String oldPassword, String newPassword) throws BaseException;

}
