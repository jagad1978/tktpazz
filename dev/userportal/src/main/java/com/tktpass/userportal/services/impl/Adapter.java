package com.tktpass.userportal.services.impl;

import java.util.List;

public interface Adapter<S,T> {
	
	public S adapt(T t);
	
	public T adaptTo(S s);
	
	public List<T> adaptTo(List<S> s);
	
	public void update(T src, S trg);

}
