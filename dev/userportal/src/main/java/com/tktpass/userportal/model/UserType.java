package com.tktpass.userportal.model;

public enum UserType {

	ADMIN, SYSTEM, USER

}
