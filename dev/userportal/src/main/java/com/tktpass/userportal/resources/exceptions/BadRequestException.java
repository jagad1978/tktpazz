package com.tktpass.userportal.resources.exceptions;

import org.springframework.http.HttpStatus;

public class BadRequestException extends BaseException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadRequestException(ErrMessage message) {
		super(message, HttpStatus.BAD_REQUEST);
	}

}
