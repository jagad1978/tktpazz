package com.tktpass.userportal.resources.exceptions;

public enum ErrMessage {
	
	UNACCEPTED_OPERATION("Unaccepted operation"),
	
	USER_NOT_FOUND("User not found"),
	USER_ALREADY_EXISTS("User with given id already exists"),
	USER_LOCKED("User with given id is locked"),
	USER_INACTIVE("User with given id is in disabled state"),
	USER_RESET("User account has been reset"),
	USER_UNKNOWN_STATUS("User account has been reset"),
	USER_ALREADY_ACTIVATED("User account was activated already"),
	AUTHENTICATION_FAILED("Username or password is invalid"),
	AUTHENTICATION_FAILED_AND_LOCKED("Username or password is invalid and hence locked"),
	USER_NOT_SELF_MANAGED("User not managed by self"),
	USER_NOT_IN_RESET_STATE("User not in reset state"),
	TOKEN_DOESNT_MATCH("Token provided doesn't match"),
	PASSWORD_DOESNT_MATCH("Old Password provided doesn't match"),
	TOKEN_EXPIRED("Token provided already expired"),
	
	ADDRESS_NOT_FOUND("Address not found"),
	NOTIFICATION_NOT_FOUND("Notification not found"),
	
	EVENT_NOT_FOUND("Event not found"),
	EVENT_NOT_CREATED("Event not found"),
	
	EVENTREQUEST_NOT_FOUND("Event not found");
	

	
	private String errorMessage;
	
	private ErrMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getMessage() {
		return errorMessage;
	}

}
