package com.tktpass.userportal.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.tktpass.userportal.services.SessionInfo;

@Component
public class AuthInfoImpl implements SessionInfo{

	@Override
	public String getUserId() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(authentication.isAuthenticated()) {
			return authentication.getName();
		}
		return null;
	}

}
