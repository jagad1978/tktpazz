package com.tktpass.userportal.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Notification {
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	@Enumerated(EnumType.STRING)
	private NotificationStatus status;
	
	@Enumerated(EnumType.STRING)
	private NotificationType type;
	
	private String message;
	
	private long createdOn;
	
	private String createdBy;
	
	private UserType creatorType;
	
	private String processor;
	
	private UserType processorType;
	
	private String source;
	
	public static Notification create(String message, NotificationType type, UserType creatorType, String createdBy, UserType processorType, String processor, String source) {
		Notification notification = new Notification();
		notification.status = NotificationStatus.UNREAD;
		notification.type = type;
		notification.message = message;
		notification.creatorType = creatorType;
		notification.createdBy = createdBy;
		notification.processorType = processorType;
		notification.processor = processor;
		notification.createdOn = new Date().getTime();
		notification.source = source;
		return notification;
	}

	public String getSource() {
		return source;
	}

	public String getId() {
		return id;
	}

	public NotificationStatus getStatus() {
		return status;
	}

	public NotificationType getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public UserType getCreatorType() {
		return creatorType;
	}

	public String getProcessor() {
		return processor;
	}

	public UserType getProcessorType() {
		return processorType;
	}

	public void setStatus(NotificationStatus status) {
		this.status = status;
	}
	
}
