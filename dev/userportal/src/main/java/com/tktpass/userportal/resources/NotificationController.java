package com.tktpass.userportal.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tktpass.userportal.model.NotificationType;
import com.tktpass.userportal.model.UserType;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.services.NotificationService;
import com.tktpass.userportal.to.NotificationCreateTO;
import com.tktpass.userportal.to.NotificationTO;

@RestController
@RequestMapping("notifications")
public class NotificationController {

	@Autowired
	private NotificationService notificationService;

	@GetMapping
	public ResponseEntity<List<NotificationTO>> getAllNotifications(
			@RequestParam(name = "onlyunread", required = false) boolean onlyunread) throws BaseException {
		List<NotificationTO> notifications = notificationService.getAllNotifications(onlyunread);
		return new ResponseEntity<List<NotificationTO>>(notifications, HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<NotificationTO> getNotification(@PathVariable("id") String notificationId) throws BaseException {
		NotificationTO notification = notificationService.getNotification(notificationId);
		return new ResponseEntity<NotificationTO>(notification, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteNotifications(@PathVariable("id") String notificationId) throws BaseException {
		boolean deleted = notificationService.deleteNotification(notificationId);
		if (deleted)
			return new ResponseEntity<String>("Successfully deleted", HttpStatus.OK);
		return new ResponseEntity<String>("Unable to delete given Notification", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@PutMapping("{id}")
	public ResponseEntity<String> markAsRead(@PathVariable("id") String notificationId) throws BaseException {
		boolean marked = notificationService.markAsRead(notificationId);
		if(marked) {
			return new ResponseEntity<String>("Successfully marked", HttpStatus.OK);
		}
		return new ResponseEntity<String>("Unable to mark Notification as Read", HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping
	public ResponseEntity<NotificationTO> createNotifications(@Valid NotificationCreateTO notification) throws BaseException {
		NotificationTO notificationTO = notificationService.createNotification(notification.getMessage(),
				NotificationType.valueOf(notification.getType()),
				UserType.valueOf(notification.getProcessorType()), notification.getProcessor(), notification.getSource());
		return new ResponseEntity<NotificationTO>(notificationTO, HttpStatus.CREATED);
	}

}
