package com.tktpass.userportal.constraints;

public class PasswordValidator extends RegexPatternValidator<ValidPassword> {

	public PasswordValidator() {
		super("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$");
	}

}
