package com.tktpass.userportal.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tktpass.userportal.model.EventType;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.services.TicketService;
import com.tktpass.userportal.to.TicketCreationTO;
import com.tktpass.userportal.to.TicketTO;

@RestController
@RequestMapping("tickets")
public class TicketController {

	@Autowired
	private TicketService service;

	@PostMapping
	public ResponseEntity<TicketTO> createTicket(@Valid @RequestBody TicketCreationTO ticketCreationTO)
			throws BaseException {
		TicketTO ticketTO = service.createTicket(ticketCreationTO);
		return new ResponseEntity<TicketTO>(ticketTO, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<List<TicketTO>> listOpenTickets(
			@RequestParam(name = "createdByMe", required = false) boolean createdByMe,
			@RequestParam(name = "eventType", required = false) String eventType,
			@RequestParam(name = "eventIds", required = false) String eventIds) throws BaseException {
		List<TicketTO> tickets = null;
		if (createdByMe) {
			tickets = service.listMyOpenTickets();
		} else if (eventType != null) {
			tickets = service.listOpenTickets(EventType.valueOf(eventType));
		} else if (eventIds != null) {
			tickets = service.listOpenTickets(eventIds.split(","));
		} else {
			tickets = service.listMyOpenTickets();
		}
		return new ResponseEntity<List<TicketTO>>(tickets, HttpStatus.OK);
	}

}
