package com.tktpass.userportal.model;

public enum UserStatus {
	
	NEW, ACTIVE, INACTIVE, LOCKED, RESET

}
