package com.tktpass.userportal.services.impl;

import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tktpass.userportal.model.Address;
import com.tktpass.userportal.model.UserProfile;
import com.tktpass.userportal.repositories.AddressRepository;
import com.tktpass.userportal.repositories.UserProfileRepository;
import com.tktpass.userportal.services.AccountService;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.resources.exceptions.ErrMessage;
import com.tktpass.userportal.services.SessionInfo;
import com.tktpass.userportal.services.UserService;
import com.tktpass.userportal.to.AddressTO;
import com.tktpass.userportal.to.UserBasicInfoTO;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);
	
	@Autowired
	private SessionInfo sessionInfo;
	
	@Autowired
	private UserProfileRepository userProfileRepository;
	
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private UserService userService;
	
	@Override
	public UserBasicInfoTO updateBasicDetails(String newName, String mobileNumber) throws BaseException {
		String userId = sessionInfo.getUserId();
		logger.info("Update basic details method is invoked for user ({}) with new name ({}) and mobileNumber ({})",
				userId, newName, mobileNumber);
		UserProfile userProfile = userProfileRepository.findOne(userId);
		logger.debug("Userprofile found to be {}", userProfile);
		if(Objects.nonNull(newName) && !newName.isEmpty() && !userProfile.getName().equals(newName)) {
			userProfile.setName(newName);
		}
		
		if(Objects.nonNull(mobileNumber) && !mobileNumber.isEmpty() && !userProfile.getMobile().equals(mobileNumber)) {
			userProfile.setMobile(mobileNumber);
		}
		userProfile = userProfileRepository.save(userProfile);
		return adaptTo(userProfile);
	}
	
	@Override
	public boolean updatePassword(String oldPassword, String newPassword) throws BaseException {
		String userId = sessionInfo.getUserId();
		return userService.updatePassword(userId, oldPassword, newPassword);
	}

	@Override
	public List<AddressTO> getAddresses() throws BaseException {
		String userId = sessionInfo.getUserId();
		UserProfile userProfile = userProfileRepository.findOne(userId);
		return new AddressAdapter().adaptTo(userProfile.getAddresses());
	}

	@Override
	public AddressTO getAddress(String addressId) throws BaseException {
		String userId = sessionInfo.getUserId();
		UserProfile userProfile = userProfileRepository.findOne(userId);
		Address address = userProfile.getAddress(addressId);
		if (address == null) {
			throw new BaseException(ErrMessage.ADDRESS_NOT_FOUND);
		}
		return new AddressAdapter().adaptTo(address);
	}

	@Override
	public String addAddress(AddressTO addressTO) throws BaseException {
		String userId = sessionInfo.getUserId();
		UserProfile userProfile = userProfileRepository.findOne(userId);
		Address address = addressRepository.save(new AddressAdapter().adapt(addressTO));
		userProfile.addAddress(address);
		return address.getId();
	}

	@Override
	public boolean updateAddress(String addressId, AddressTO addressTO) throws BaseException {
		if(!addressRepository.exists(addressTO.getId())) {
			throw new BaseException(ErrMessage.ADDRESS_NOT_FOUND);
		};
		String userId = sessionInfo.getUserId();
		UserProfile userProfile = userProfileRepository.findOne(userId);
		Address address = userProfile.getAddress(addressId);
		if (address == null) {
			throw new BaseException(ErrMessage.ADDRESS_NOT_FOUND);
		}
		new AddressAdapter().update(addressTO, address);
		addressRepository.save(address);
		return true;
	}
	
	private UserBasicInfoTO adaptTo(UserProfile profile) {
		return new UserBasicInfoTO(profile.getName(), profile.getMobile());
	}

}
