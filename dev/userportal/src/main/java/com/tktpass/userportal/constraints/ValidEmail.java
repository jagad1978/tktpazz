package com.tktpass.userportal.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy=EmailValidator.class)
@Documented
public @interface ValidEmail {
    String message() default "{valid email}";
    Class<?>[] groups() default {};
    Class<?>[] payload() default {};

}
