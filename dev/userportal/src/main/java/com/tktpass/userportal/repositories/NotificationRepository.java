package com.tktpass.userportal.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tktpass.userportal.model.Notification;
import com.tktpass.userportal.model.NotificationStatus;

public interface NotificationRepository extends CrudRepository<Notification, String> {

	@Query("SELECT t FROM Notification t where t.processor = :processor")
	public List<Notification> findByProcessor(@Param("processor") String processor);

	@Query("SELECT t FROM Notification t where t.processor = :processor and t.status = :status")
	public List<Notification> findByProcessor(@Param("processor") String processor,
			@Param("status") NotificationStatus status);

}
