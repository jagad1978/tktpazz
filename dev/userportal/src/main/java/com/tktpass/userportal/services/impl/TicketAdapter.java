package com.tktpass.userportal.services.impl;

import java.util.ArrayList;
import java.util.List;

import com.tktpass.userportal.model.Ticket;
import com.tktpass.userportal.to.TicketTO;

public class TicketAdapter implements Adapter<Ticket, TicketTO> {

	@Override
	public Ticket adapt(TicketTO t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TicketTO adaptTo(Ticket ticket) {
		TicketTO ticketTO = new TicketTO();
		ticketTO.setId(ticket.getId());
		ticketTO.setDescription(ticket.getDescription());
		ticketTO.setEventId(ticket.getEventId());
		ticketTO.setListedBy(ticket.getListedBy());
		ticketTO.setParentId(ticket.getParentId());
		ticketTO.setStatus(ticket.getStatus().name());
		ticketTO.setValue(ticket.getValue());
		ticketTO.setCurrency(ticket.getCurrency().name());
		ticketTO.setSeats(ticket.getSeats());
		return ticketTO;
	}

	@Override
	public List<TicketTO> adaptTo(List<Ticket> tickets) {
		List<TicketTO> result = new ArrayList<>(tickets.size());
		for (Ticket ticket : tickets) {
			result.add(adaptTo(ticket));
		}
		return result;
	}

	@Override
	public void update(TicketTO src, Ticket trg) {
		// TODO Auto-generated method stub
	}

}
