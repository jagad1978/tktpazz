package com.tktpass.userportal.to;

public class UserCreationTO {
	
	private String name;
	
	private String userId;
	
	private String emailId;
	
	private long createdOn;

	//FIXME Ideally there should be mail server to perform the same
	private String token;
	
	public UserCreationTO(String name, String userId, String emailId, long createdOn) {
		this.name = name;
		this.userId = userId;
		this.emailId = emailId;
		this.createdOn = createdOn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
