package com.tktpass.userportal.to;

public class EventTO {
	
	private String id;
	
	private String name;
	
	private long startTime;
	
	private long duration;
	
	private AddressTO addressTO;
	
	private String eventType;
	
	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public EventTO() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public AddressTO getAddressTO() {
		return addressTO;
	}

	public void setAddressTO(AddressTO addressTO) {
		this.addressTO = addressTO;
	}

}
