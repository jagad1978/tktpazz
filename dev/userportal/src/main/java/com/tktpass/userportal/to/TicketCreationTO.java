package com.tktpass.userportal.to;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class TicketCreationTO {
	
	@NotNull
	@NotEmpty
	private String eventId;

	private double value;

	@NotNull
	@NotEmpty
	private String description;

	@NotNull
	@NotEmpty
	private String currency;

	@NotNull
	@NotEmpty
	private List<String> seats;

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public List<String> getSeats() {
		return seats;
	}

	public void setSeats(List<String> seats) {
		this.seats = seats;
	}

}
