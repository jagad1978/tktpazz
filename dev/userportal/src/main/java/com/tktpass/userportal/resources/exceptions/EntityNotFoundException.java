package com.tktpass.userportal.resources.exceptions;

import org.springframework.http.HttpStatus;

public class EntityNotFoundException extends BaseException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public EntityNotFoundException(ErrMessage message) {
		super(message, HttpStatus.NOT_FOUND);
	}

}
