package com.tktpass.userportal.to;

public class EventRequestTO {
	
	private String eventName;
	
	private String eventId;
	
	private String eventRequestId;
	
	private long startTime;
	
	private String status;

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getEventRequestId() {
		return eventRequestId;
	}

	public void setEventRequestId(String eventRequestId) {
		this.eventRequestId = eventRequestId;
	}

}
