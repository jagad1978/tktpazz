package com.tktpass.userportal.services.impl;

import java.util.ArrayList;
import java.util.List;

import com.tktpass.userportal.model.Address;
import com.tktpass.userportal.to.AddressTO;

public class AddressAdapter implements Adapter<Address, AddressTO> {

	@Override
	public Address adapt(AddressTO addressTO) {
		return new Address(addressTO.getName(), addressTO.getAddressLine1(), addressTO.getAddressLine2(),
				addressTO.getCity(), addressTO.getState(), addressTO.getCountry(), addressTO.getMobileNumber(),
				addressTO.getZipcode(), addressTO.getLandmark());
	}

	@Override
	public AddressTO adaptTo(Address address) {
		AddressTO addressTO = new AddressTO();
		addressTO.setId(address.getId());
		addressTO.setName(address.getName());
		addressTO.setAddressLine1(address.getAddressLine1());
		addressTO.setAddressLine2(address.getAddressLine2());
		addressTO.setCity(address.getCity());
		addressTO.setState(address.getState());
		addressTO.setCountry(address.getCountry());
		addressTO.setLandmark(address.getLandmark());
		addressTO.setMobileNumber(address.getMobile());
		addressTO.setZipcode(address.getZipcode());
		return addressTO;
	}

	@Override
	public List<AddressTO> adaptTo(List<Address> addresses) {
		List<AddressTO> addressTOs = new ArrayList<>();
		for (Address address : addresses) {
			addressTOs.add(adaptTo(address));
		}
		return addressTOs;
	}

	@Override
	public void update(AddressTO src, Address trg) {
		trg.setName(src.getName());
		trg.setAddressLine1(src.getAddressLine1());
		trg.setAddressLine2(src.getAddressLine2());
		trg.setCity(src.getCity());
		trg.setState(src.getState());
		trg.setCountry(src.getCountry());
		trg.setLandmark(src.getLandmark());
		trg.setMobile(src.getMobileNumber());
		trg.setZipcode(src.getZipcode());
	}

}
