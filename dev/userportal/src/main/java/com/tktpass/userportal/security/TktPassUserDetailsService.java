package com.tktpass.userportal.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.tktpass.userportal.model.UserPermission;
import com.tktpass.userportal.model.UserRole;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.services.UserService;
import com.tktpass.userportal.to.UserInfoTO;

@Service
public class TktPassUserDetailsService implements UserDetailsService {

	@Autowired
	private UserService service;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserInfoTO userInfo = null;
		try {
			userInfo = service.getUserInfo(username);
		} catch (BaseException e) {
			throw new UsernameNotFoundException("User Not found", e);
		}
		return new User(userInfo.getUserName(), userInfo.getPassword(), userInfo.isEnabled(),
				userInfo.isAccountNotExpired(), userInfo.isCredentialsNotExpired(), userInfo.isAccountNotLocked(),
				getAuthorities(userInfo.getRole()));
	}

	private static List<GrantedAuthority> getAuthorities(String role) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		Set<UserPermission> permissions = UserRole.valueOf(role).getPermissions();
		for (UserPermission permission : permissions) {
			authorities.add(new SimpleGrantedAuthority(permission.name()));
		}
		return authorities;
	}

}
