package com.tktpass.userportal.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tktpass.userportal.events.NotificationEvent;
import com.tktpass.userportal.model.Notification;
import com.tktpass.userportal.model.NotificationStatus;
import com.tktpass.userportal.model.NotificationType;
import com.tktpass.userportal.model.UserType;
import com.tktpass.userportal.repositories.NotificationRepository;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.resources.exceptions.EntityNotFoundException;
import com.tktpass.userportal.resources.exceptions.ErrMessage;
import com.tktpass.userportal.services.NotificationService;
import com.tktpass.userportal.services.SessionInfo;
import com.tktpass.userportal.to.NotificationTO;

@Service
@Transactional
public class NotificationServiceImpl implements NotificationService, ApplicationListener<NotificationEvent> {

	@Autowired
	private SessionInfo sessionInfo;

	@Autowired
	private NotificationRepository repository;

	private static final Logger logger = LoggerFactory.getLogger(NotificationServiceImpl.class);

	@Override
	public List<NotificationTO> getAllNotifications(boolean onlyUnread) {
		String userId = sessionInfo.getUserId();
		List<Notification> notifications = null;
		if (onlyUnread) {
			notifications = repository.findByProcessor(userId, NotificationStatus.UNREAD);
		} else {
			notifications = repository.findByProcessor(userId);
		}
		return adaptTo(notifications);
	}

	@Override
	public NotificationTO getNotification(String notificationId) throws BaseException {
		Notification notification = repository.findOne(notificationId);
		if (notification == null) {
			throw new EntityNotFoundException(ErrMessage.NOTIFICATION_NOT_FOUND);
		}
		return adaptTo(notification);
	}

	@Override
	public NotificationTO createNotification(String message, NotificationType type, UserType processorType,
			String processor, String source) throws BaseException {
		String userId = sessionInfo.getUserId();
		Notification notification = Notification.create(message, type, UserType.USER, userId, processorType, processor,
				source);
		notification = repository.save(notification);
		return adaptTo(notification);
	}

	@Override
	public boolean markAsRead(String notificationId) throws BaseException {
		Notification notification = repository.findOne(notificationId);
		if (notification == null) {
			throw new EntityNotFoundException(ErrMessage.NOTIFICATION_NOT_FOUND);
		}
		notification.setStatus(NotificationStatus.READ);
		repository.save(notification);
		return true;
	}

	@Override
	public boolean deleteNotification(String notificationId) throws BaseException {
		Notification notification = repository.findOne(notificationId);
		if (notification == null) {
			throw new EntityNotFoundException(ErrMessage.NOTIFICATION_NOT_FOUND);
		}
		repository.delete(notificationId);
		return true;
	}

	private List<NotificationTO> adaptTo(List<Notification> notifications) {
		List<NotificationTO> result = new ArrayList<>();
		for (Notification notification : notifications) {
			result.add(adaptTo(notification));
		}
		return result;
	}

	private NotificationTO adaptTo(Notification notification) {
		NotificationTO notTO = new NotificationTO();
		notTO.setId(notification.getId());
		notTO.setCreatedBy(notification.getCreatedBy());
		notTO.setCreatedOn(notification.getCreatedOn());
		notTO.setCreatorType(notification.getCreatorType().name());
		notTO.setMessage(notification.getMessage());
		notTO.setNotificationStatus(notification.getStatus().name());
		notTO.setNotificationType(notification.getType().name());
		notTO.setProcessor(notification.getProcessor());
		notTO.setProcessorType(notification.getProcessorType().name());
		return notTO;
	}

	@Override
	public void onApplicationEvent(NotificationEvent event) {
		String userId = sessionInfo.getUserId();
		Notification notification = Notification.create(event.getMessage(), event.getType(), UserType.USER, userId,
				event.getProcessorType(), event.getProcessorId(), (String) event.getSource());
		repository.save(notification);
	}

}
