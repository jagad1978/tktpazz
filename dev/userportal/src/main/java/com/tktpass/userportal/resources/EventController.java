package com.tktpass.userportal.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tktpass.userportal.model.EventType;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.resources.exceptions.ErrMessage;
import com.tktpass.userportal.services.EventService;
import com.tktpass.userportal.to.EventCreationTO;
import com.tktpass.userportal.to.EventRequestTO;
import com.tktpass.userportal.to.EventTO;

@RestController
public class EventController {

	@Autowired
	private EventService eventService;

	@PostMapping("eventrequests")
	public ResponseEntity<EventRequestTO> requestEvent(@Valid @RequestBody EventCreationTO eventCreationTO)
			throws BaseException {
		EventRequestTO requestEvent = eventService.requestEvent(eventCreationTO);
		return new ResponseEntity<EventRequestTO>(requestEvent, HttpStatus.CREATED);
	};

	@GetMapping("eventrequests")
	public ResponseEntity<List<EventRequestTO>> listEventRequests() throws BaseException {
		List<EventRequestTO> eventRequests = eventService.listEventRequests();
		return new ResponseEntity<List<EventRequestTO>>(eventRequests, HttpStatus.OK);
	};

	@PutMapping("eventrequests/{id}")
	public ResponseEntity<String> sendReminder(@PathVariable("id") String eventRequestId,
			@RequestParam("message") String message, @RequestParam("action") String action) throws BaseException {
		if("remind".equals(action)) {
			eventService.sendReminder(eventRequestId, message);
			return new ResponseEntity<String>("Event successfully reminded", HttpStatus.OK);
		}else if("approve".equals(action)) {
			eventService.approve(eventRequestId);
			return new ResponseEntity<String>("Event successfully approved", HttpStatus.OK);	
		}
		throw new BaseException(ErrMessage.UNACCEPTED_OPERATION);
	};

	@GetMapping("events")
	public ResponseEntity<List<EventTO>> listEvents(@RequestParam(name = "types", required = false) String types)
			throws BaseException {
		List<EventTO> events = null;
		if (types == null) {
			events = eventService.listFutureEvents();
		} else {
			String[] splitTypes = types.split(",");
			EventType[] eventTypes = new EventType[splitTypes.length];
			for (int i = 0; i < eventTypes.length; i++) {
				eventTypes[i] = EventType.valueOf(splitTypes[i]);
			}
			events = eventService.listFutureEvents(eventTypes);
		}
		return new ResponseEntity<List<EventTO>>(events, HttpStatus.OK);
	};

	@GetMapping("events/{id}")
	public ResponseEntity<EventTO> getEvent(@PathVariable("id") String id) throws BaseException {
		EventTO eventTO = eventService.getEvent(id);
		return new ResponseEntity<EventTO>(eventTO, HttpStatus.OK);
	};

}
