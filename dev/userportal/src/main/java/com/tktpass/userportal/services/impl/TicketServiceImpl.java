package com.tktpass.userportal.services.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tktpass.userportal.model.Currency;
import com.tktpass.userportal.model.Event;
import com.tktpass.userportal.model.EventType;
import com.tktpass.userportal.model.Ticket;
import com.tktpass.userportal.repositories.EventRepository;
import com.tktpass.userportal.repositories.TicketRepository;
import com.tktpass.userportal.services.SessionInfo;
import com.tktpass.userportal.services.TicketService;
import com.tktpass.userportal.to.TicketCreationTO;
import com.tktpass.userportal.to.TicketTO;

@Service
@Transactional
public class TicketServiceImpl implements TicketService {

	@Autowired
	private SessionInfo sessionInfo;

	@Autowired
	private TicketRepository ticketRepository;

	@Autowired
	private EventRepository eventRepository;

	@Override
	public TicketTO createTicket(TicketCreationTO ticketCreationTo) {
		String userId = sessionInfo.getUserId();
		Ticket ticket = Ticket.createTicket(ticketCreationTo.getEventId(), ticketCreationTo.getDescription(),
				ticketCreationTo.getValue(), Currency.valueOf(ticketCreationTo.getCurrency()),
				ticketCreationTo.getSeats(), userId);
		return getAdapter().adaptTo(ticketRepository.save(ticket));
	}

	private TicketAdapter getAdapter() {
		return new TicketAdapter();
	}

	@Override
	public List<TicketTO> listMyOpenTickets() {
		String userId = sessionInfo.getUserId();
		List<Ticket> tickets = ticketRepository.listNewTicketsByUser(userId);
		return getAdapter().adaptTo(tickets);
	}

	@Override
	public List<TicketTO> listOpenTickets() {
		List<Ticket> tickets = ticketRepository.listNewTickets();
		return getAdapter().adaptTo(tickets);
	}

	@Override
	public List<TicketTO> listOpenTickets(EventType eventType) {
		List<Event> events = eventRepository.listFutureEventsByType(eventType, new Date().getTime());
		String[] eventIds = new String[events.size()];
		for (int i = 0; i < events.size(); i++) {
			eventIds[i] = events.get(i).getId();
		}
		return listOpenTickets(eventIds);
	}

	@Override
	public List<TicketTO> listOpenTickets(String eventId) {
		List<Ticket> tickets = ticketRepository.listNewTicketsForEvent(eventId);
		return getAdapter().adaptTo(tickets);
	}

	@Override
	public List<TicketTO> listOpenTickets(String... eventIds) {
		List<Ticket> tickets = ticketRepository.listNewTicketsForEvents(Arrays.asList(eventIds));
		Collections.sort(tickets, new Comparator<Ticket>() {
			@Override
			public int compare(Ticket t1, Ticket t2) {
				return t1.getEventId().compareTo(t2.getEventId());
			}
		});
		return getAdapter().adaptTo(tickets);
	}

}
