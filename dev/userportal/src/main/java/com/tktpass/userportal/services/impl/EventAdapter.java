package com.tktpass.userportal.services.impl;

import java.util.ArrayList;
import java.util.List;

import com.tktpass.userportal.model.Event;
import com.tktpass.userportal.to.EventTO;

public class EventAdapter implements Adapter<Event, EventTO> {

	@Override
	public Event adapt(EventTO eventTO) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EventTO adaptTo(Event event) {
		EventTO eventTO = new EventTO();
		eventTO.setId(event.getId());
		eventTO.setName(event.getName());
		eventTO.setStartTime(event.getStartTime());
		eventTO.setDuration(event.getDuration());
		eventTO.setEventType(event.getType().name());
		eventTO.setAddressTO(new AddressAdapter().adaptTo(event.getAddress()));
		return eventTO;
	}

	@Override
	public List<EventTO> adaptTo(List<Event> events) {
		List<EventTO> eventTOs = new ArrayList<>();
		for (Event event : events) {
			eventTOs.add(adaptTo(event));
		}
		return eventTOs;
	}

	@Override
	public void update(EventTO src, Event trg) {
		// TODO Auto-generated method stub

	}

}
