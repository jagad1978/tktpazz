package com.tktpass.userportal.model;

public enum EventRequestStatus {
	
	APPROVED, NOT_APPROVED, NEW, REVIEW

}
