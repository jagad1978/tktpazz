package com.tktpass.userportal.resources;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tktpass.userportal.constraints.ValidEmail;
import com.tktpass.userportal.constraints.ValidPassword;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.services.UserService;
import com.tktpass.userportal.to.UserCreationTO;
import com.tktpass.userportal.to.UserSignUpTO;

@RestController
@RequestMapping("/users")
// FIXME logging
// FIXME Externalization
// FIXME Performance aspects
public class UserController {

	@Autowired
	private UserService service;

	@PostMapping
	public ResponseEntity<UserCreationTO> signUp(@Valid UserSignUpTO userSignUpTO) throws BaseException {
		return new ResponseEntity<UserCreationTO>(service.signUp(userSignUpTO), HttpStatus.CREATED);
	}

	@PutMapping("/{userId}/status")
	public ResponseEntity<?> confirmRegistration(@PathVariable("userId") String userId, @RequestParam String token)
			throws BaseException {
		service.confirmRegistration(userId, token);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("/{userId}/password")
	public ResponseEntity<?> forgetPassword(@PathVariable("userId") @ValidEmail String emailId) throws BaseException {
		String resetId = service.forgetPassword(emailId);
		return new ResponseEntity<String>(String
				.format("User password is successfully reset(%s). Email has been sent to the given mail id", resetId),
				HttpStatus.OK);
	}

	@PutMapping("/{userId}/password")
	public ResponseEntity<?> resetPassword(@PathVariable("userId") String userId, @RequestParam String resetId,
			@RequestParam("password") @ValidPassword String newPassword) throws BaseException {
		service.resetPassword(userId, resetId, newPassword);
		return new ResponseEntity<String>("User password successfully changed. Please login again", HttpStatus.OK);
	}

}
