package com.tktpass.userportal.services;

import java.util.List;

import com.tktpass.userportal.model.EventType;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.to.EventCreationTO;
import com.tktpass.userportal.to.EventRequestTO;
import com.tktpass.userportal.to.EventTO;

public interface EventService {

	public EventRequestTO requestEvent(EventCreationTO eventCreationTO) throws BaseException;

	public List<EventRequestTO> listEventRequests() throws BaseException;

	public void sendReminder(String eventRequestId, String message) throws BaseException;
	
	public void approve(String eventRequestId) throws BaseException;

	public List<EventTO> listFutureEvents(EventType... eventTypes) throws BaseException;

	public EventTO getEvent(String id) throws BaseException;

}
