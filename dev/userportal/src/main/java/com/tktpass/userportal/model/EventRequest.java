package com.tktpass.userportal.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class EventRequest {
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	private String createdBy;
	
	private long createdOn;
	
	@OneToOne
	private Event event;
	
	@Enumerated(EnumType.STRING)
	private EventRequestStatus status;
	
	public static EventRequest createEventRequest(Event event, String createdBy, long createdOn) {
		EventRequest request = new EventRequest();
		request.event = event;
		request.createdBy = createdBy;
		request.createdOn = createdOn;
		request.status = EventRequestStatus.NEW;
		return request;
	}

	public String getId() {
		return id;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public Event getEvent() {
		return event;
	}

	public EventRequestStatus getStatus() {
		return status;
	}

	public void setStatus(EventRequestStatus status) {
		this.status = status;
	}

}
