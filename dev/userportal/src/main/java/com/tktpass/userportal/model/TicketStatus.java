package com.tktpass.userportal.model;

public enum TicketStatus {
	
	NEW, SOLD, INACTIVE

}
