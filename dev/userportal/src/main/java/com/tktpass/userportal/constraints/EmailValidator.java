package com.tktpass.userportal.constraints;

public class EmailValidator extends  RegexPatternValidator<ValidEmail> {
	
	public EmailValidator() {
		super("^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$");
	}

}
