package com.tktpass.userportal.model;

public enum EventType {
	
	MUSIC, SPORTS, ARTS_THEATRE, FAMILY

}
