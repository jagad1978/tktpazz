package com.tktpass.userportal.services.impl;

import java.util.ArrayList;
import java.util.List;

import com.tktpass.userportal.model.Event;
import com.tktpass.userportal.model.EventRequest;
import com.tktpass.userportal.to.EventRequestTO;

public class EventRequestAdapter implements Adapter<EventRequest, EventRequestTO> {

	@Override
	public EventRequest adapt(EventRequestTO t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EventRequestTO adaptTo(EventRequest eventRequest) {
		EventRequestTO to = new EventRequestTO();
		Event event = eventRequest.getEvent();
		to.setEventName(event.getName());
		to.setStartTime(event.getStartTime());
		to.setEventId(event.getId());
		to.setEventRequestId(eventRequest.getId());
		to.setStatus(eventRequest.getStatus().name());
		return to;
	}

	@Override
	public List<EventRequestTO> adaptTo(List<EventRequest> eventRequests) {
		List<EventRequestTO> eventRequestTOs = new ArrayList<>();
		for (EventRequest req : eventRequests) {
			eventRequestTOs.add(adaptTo(req));
		}
		return eventRequestTOs;
	}

	@Override
	public void update(EventRequestTO src, EventRequest trg) {
		// TODO Auto-generated method stub
		
	}

}
