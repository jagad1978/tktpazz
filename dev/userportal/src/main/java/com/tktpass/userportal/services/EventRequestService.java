package com.tktpass.userportal.services;

public interface EventRequestService {
	
	public void createEventRequest();
	
	public void remindEventRequest(String requestId);

}
