package com.tktpass.userportal.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public enum UserRole {

	USER(UserPermission.EVENT_REQUEST), ADMIN(UserPermission.APPROVE_EVENT_REQUEST);

	private final Set<UserPermission> permissions;

	private UserRole(UserPermission... permissions) {
		if (permissions == null || permissions.length == 0) {
			throw new IllegalArgumentException("Permissions cannot be empty");
		}
		this.permissions = new HashSet<>(permissions.length);
		for (UserPermission permission : permissions) {
			this.permissions.add(permission);
		}
	}

	public Set<UserPermission> getPermissions() {
		return Collections.unmodifiableSet(this.permissions);
	}
	
	

}
