package com.tktpass.userportal.services.impl;

import java.util.Date;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.tktpass.userportal.model.LoginAgent;
import com.tktpass.userportal.model.UserLogin;
import com.tktpass.userportal.model.UserProfile;
import com.tktpass.userportal.model.UserStatus;
import com.tktpass.userportal.repositories.UserLoginRepository;
import com.tktpass.userportal.repositories.UserProfileRepository;
import com.tktpass.userportal.resources.exceptions.BadRequestException;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.resources.exceptions.EntityAlreadyExistsException;
import com.tktpass.userportal.resources.exceptions.EntityNotFoundException;
import com.tktpass.userportal.resources.exceptions.ErrMessage;
import com.tktpass.userportal.services.UserService;
import com.tktpass.userportal.to.UserCreationTO;
import com.tktpass.userportal.to.UserInfoTO;
import com.tktpass.userportal.to.UserSignUpTO;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	private static final int USER_RESET_EXPIRY_MINS = 100;

	@Autowired
	private UserLoginRepository userLoginRepository;

	@Autowired
	private UserProfileRepository userProfileRepository;

	@Autowired
	private PasswordEncoder encoder;

	@Override
	public UserCreationTO signUp(UserSignUpTO userSignUpTO) throws BaseException {
		// FIXME we need to send mail to activate
		// Check User name already exists
		UserLogin user = userLoginRepository.findOne(userSignUpTO.getUserId());
		if (user != null) {
			throw new EntityAlreadyExistsException(ErrMessage.USER_ALREADY_EXISTS);
		}
		// secure password
		String saltedPassword = encoder.encode(userSignUpTO.getPassword());

		// Create UserLogin
		UserLogin userLogin = UserLogin.signUp(userSignUpTO.getUserId(), saltedPassword);
		UserLogin savedUser = userLoginRepository.save(userLogin);

		// Create UserProfile
		UserProfile profile = UserProfile.signUp(userSignUpTO.getUserId(), userSignUpTO.getName(),
				userSignUpTO.getEmailId(), userSignUpTO.getMobile());
		UserProfile savedProfile = userProfileRepository.save(profile);
		UserCreationTO to = new UserCreationTO(savedProfile.getName(), savedProfile.getUserId(),
				savedProfile.getEmailId(), savedUser.getCreatedOn());
		to.setToken(userLogin.getToken());
		return to;
	}

	@Override
	public UserInfoTO getUserInfo(String userName) throws BaseException {
		// Check User name already exists
		UserLogin userLogin = userLoginRepository.findOne(userName);
		if (userLogin == null) {
			throw new EntityNotFoundException(ErrMessage.USER_NOT_FOUND);
		}
		UserInfoTO user = new UserInfoTO();
		user.setUserName(userName);
		user.setPassword(userLogin.getPassword());
		user.setEnabled(userLogin.getStatus() == UserStatus.ACTIVE);
		user.setAccountNotExpired(userLogin.getStatus() != UserStatus.INACTIVE);
		user.setAccountNotLocked(userLogin.getStatus() != UserStatus.LOCKED);
		user.setCredentialsNotExpired(userLogin.getStatus() != UserStatus.NEW);
		user.setRole(userLogin.getRole().name());
		return user;
	}

	@Override
	public String forgetPassword(String emailId) throws BaseException {
		UserProfile userProfile = userProfileRepository.findByEmailId(emailId);
		if (userProfile == null) {
			throw new BadRequestException(ErrMessage.USER_NOT_FOUND);
		}

		UserLogin userLogin = userLoginRepository.findOne(userProfile.getUserId());
		if (userLogin == null) {
			throw new BaseException(ErrMessage.USER_NOT_FOUND);
		}

		if (LoginAgent.SELF != userLogin.getAgent()) {
			throw new BaseException(ErrMessage.USER_NOT_SELF_MANAGED);
		}

		String resetId = generateRandomId();
		new MailNotificationManager().sendEmailForPasswordReset(userProfile.getName(), emailId, resetId);
		userLogin.setToken(resetId);
		userLogin.setResetOn(new Date().getTime());
		userLogin.setStatus(UserStatus.RESET);
		userLoginRepository.save(userLogin);
		return resetId;
	}

	private String generateRandomId() {
		// FIXME provide better string
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	@Override
	public boolean confirmRegistration(String userId, String token) throws BaseException {
		UserLogin userLogin = userLoginRepository.findOne(userId);
		if (userLogin == null) {
			throw new BadRequestException(ErrMessage.USER_NOT_FOUND);
		}

		if (UserStatus.NEW != userLogin.getStatus()) {
			throw new BadRequestException(ErrMessage.USER_ALREADY_ACTIVATED);
		}

		if (!token.equals(userLogin.getToken())) {
			throw new BadRequestException(ErrMessage.TOKEN_DOESNT_MATCH);
		}

		userLogin.setStatus(UserStatus.ACTIVE);
		userLogin.setToken(null);
		userLoginRepository.save(userLogin);
		return true;
	}

	@Override
	public boolean resetPassword(String userId, String resetId, String newPassword) throws BaseException {
		UserLogin userLogin = userLoginRepository.findOne(userId);
		if (userLogin == null) {
			throw new BadRequestException(ErrMessage.USER_NOT_FOUND);
		}

		if (UserStatus.RESET != userLogin.getStatus()) {
			throw new BadRequestException(ErrMessage.USER_NOT_FOUND);
		}

		if (!resetId.equals(userLogin.getToken())) {
			throw new BadRequestException(ErrMessage.TOKEN_DOESNT_MATCH);
		}

		if (new Date().getTime() - userLogin.getResetOn() > USER_RESET_EXPIRY_MINS * 60 * 1000) {
			throw new BadRequestException(ErrMessage.TOKEN_EXPIRED);
		}

		String securePassword = encoder.encode(newPassword);
		userLogin.setPassword(securePassword);
		userLogin.setStatus(UserStatus.ACTIVE);
		userLogin.setResetOn(0);
		userLoginRepository.save(userLogin);
		return true;
	}
	
	@Override
	public boolean updatePassword(String userId, String oldPassword, String newPassword) throws BaseException {
		UserLogin userLogin = userLoginRepository.findOne(userId);
		if (userLogin == null) {
			throw new EntityNotFoundException(ErrMessage.USER_NOT_FOUND);
		}
		String storedPassword = userLogin.getPassword();
		boolean matches = encoder.matches(oldPassword, storedPassword);
		if(!matches) {
			throw new BadRequestException(ErrMessage.PASSWORD_DOESNT_MATCH);
		}
		userLogin.setPassword(encoder.encode(newPassword));
		return true;
	}

}
