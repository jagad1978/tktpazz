package com.tktpass.userportal.to;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.tktpass.userportal.constraints.ValidEmail;
import com.tktpass.userportal.constraints.ValidMobileNumber;
import com.tktpass.userportal.constraints.ValidPassword;

public class UserSignUpTO {
	
	@NotNull
	@NotEmpty
	private String userId;
	
	@NotNull
	@NotEmpty
	@ValidPassword
	private String password;

	@NotNull
	@NotEmpty
	@ValidEmail
	private String emailId;
	
	@NotNull
	@NotEmpty
	private String name;
	
	@NotNull
	@NotEmpty
	@ValidMobileNumber
	private String mobile;
	

	public String getUserId() {
		return userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public String getName() {
		return name;
	}

	public String getMobile() {
		return mobile;
	}
	
	public String getPassword() {
		return password;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

}
