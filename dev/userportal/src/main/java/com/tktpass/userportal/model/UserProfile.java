package com.tktpass.userportal.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserProfile {

	@Id
	private String userId;

	private String name;

	private String emailId;

	private String mobile;
	
	@ElementCollection
	private final List<Address> addresses;

	public UserProfile() {
		this.addresses = new ArrayList<>();
	}

	public UserProfile(String userId) {
		this.userId = userId;
		this.addresses = new ArrayList<>();
	}

	public static UserProfile signUp(String userId, String name, String emailId, String mobile) {
		UserProfile profile = new UserProfile(userId);
		profile.name = name;
		profile.emailId = emailId;
		profile.mobile = mobile;
		return profile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getUserId() {
		return userId;
	}

	public List<Address> getAddresses() {
		return Collections.unmodifiableList(addresses);
	}
	
	public Address getAddress(String id) {
		for(Address address: addresses) {
			if(address.getId().equals(id)) {
				return address;
			}
		}
		return null;
	}
	
	public void addAddress(Address address) {
		addresses.add(address);
	}

	@Override
	public String toString() {
		return "UserProfile [userId=" + userId + ", name=" + name + "]";
	}

}
