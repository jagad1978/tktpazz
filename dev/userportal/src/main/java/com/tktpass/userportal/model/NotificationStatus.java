package com.tktpass.userportal.model;

public enum NotificationStatus {
	
	READ, UNREAD;

}
