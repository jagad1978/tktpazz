package com.tktpass.userportal.to;

public class UserBasicInfoTO {
	
	private String name;
	
	private String mobileId;
	
	public UserBasicInfoTO(String name, String mobileId) {
		super();
		this.name = name;
		this.mobileId = mobileId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileId() {
		return mobileId;
	}

	public void setMobileId(String mobileId) {
		this.mobileId = mobileId;
	}

}
