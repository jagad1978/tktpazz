package com.tktpass.userportal.resources;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.tktpass.userportal.resources.exceptions.BaseException;

@ControllerAdvice
public class BaseExceptionHandler {

	@ExceptionHandler(BaseException.class)
	public ModelAndView handleBaseException(BaseException ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setStatus(ex.getStatus());
		modelAndView.addObject("code", ex.getErrMessage().name());
		modelAndView.addObject("message", ex.getErrMessage().getMessage());
		return modelAndView;
	}

}
