package com.tktpass.userportal.to;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class NotificationCreateTO {

	@NotNull
	@NotEmpty
	private String message;
	
	@NotNull
	@NotEmpty
	private String type;
	
	@NotNull
	@NotEmpty
	private String source;
	
	@NotNull
	@NotEmpty
	private String processorType;
	
	private String processor;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProcessorType() {
		return processorType;
	}

	public void setProcessorType(String processorType) {
		this.processorType = processorType;
	}

	public String getProcessor() {
		return processor;
	}

	public void setProcessor(String processor) {
		this.processor = processor;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
