package com.tktpass.userportal.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tktpass.userportal.model.EventRequest;

public interface EventRequestRepository extends CrudRepository<EventRequest, String>{
	
	@Query("SELECT t FROM EventRequest t where t.createdBy = :userId")
	public List<EventRequest> listByCreator(@Param("userId") String userId);

}
