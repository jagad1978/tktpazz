package com.tktpass.userportal.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.tktpass.userportal.model.Event;
import com.tktpass.userportal.model.EventType;

public interface EventRepository extends CrudRepository<Event, String>{
	
	@Query("SELECT t FROM Event t where t.type = :type and t.startTime > :currentTime and t.status = 'ACTIVE'")
	public List<Event> listFutureEventsByType(@Param("type") EventType type, @Param("currentTime") long currentTime);
	
	@Query("SELECT t FROM Event t where t.startTime > :currentTime and t.status = 'ACTIVE'")
	public List<Event> listFutureEvents(@Param("currentTime") long currentTime);
	
	@Query("SELECT t FROM Event t where t.type in :types and t.startTime > :currentTime and t.status = 'ACTIVE'")
	public List<Event> listFutureEventsByTypes(@Param("types") List<EventType> types, @Param("currentTime") long currentTime);

}
