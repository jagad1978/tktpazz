package com.tktpass.userportal.services;

import java.util.List;

import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.to.AddressTO;
import com.tktpass.userportal.to.UserBasicInfoTO;

public interface AccountService {

	UserBasicInfoTO updateBasicDetails(String userName, String mobileNumber) throws BaseException;

	List<AddressTO> getAddresses() throws BaseException;

	AddressTO getAddress(String addressId) throws BaseException;

	String addAddress(AddressTO addressTO) throws BaseException;

	boolean updateAddress(String addressId, AddressTO addressTO) throws BaseException;
	
	boolean updatePassword(String oldPassword, String newPassword) throws BaseException;

}
