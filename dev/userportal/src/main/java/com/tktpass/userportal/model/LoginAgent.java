package com.tktpass.userportal.model;

public enum LoginAgent {
	
	SELF, FACEBOOK, GOOGLE

}
