package com.tktpass.userportal.constraints;

public class MobileNumberValidator extends  RegexPatternValidator<ValidEmail> {

	public MobileNumberValidator() {
		super("[0-9]{10}");
	}

}
