package com.tktpass.userportal.services;

import java.util.List;

import com.tktpass.userportal.model.NotificationType;
import com.tktpass.userportal.model.UserType;
import com.tktpass.userportal.resources.exceptions.BaseException;
import com.tktpass.userportal.to.NotificationTO;

public interface NotificationService {

	public List<NotificationTO> getAllNotifications(boolean onlyUnread) throws BaseException;

	public NotificationTO getNotification(String notificationId) throws BaseException;

	public NotificationTO createNotification(String message, NotificationType type, UserType processorType,
			String processor, String source) throws BaseException;

	public boolean markAsRead(String notificationId) throws BaseException;

	public boolean deleteNotification(String notificationId) throws BaseException;

}
