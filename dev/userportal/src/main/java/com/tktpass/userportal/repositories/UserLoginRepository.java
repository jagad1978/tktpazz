package com.tktpass.userportal.repositories;

import org.springframework.data.repository.CrudRepository;

import com.tktpass.userportal.model.UserLogin;

public interface UserLoginRepository extends CrudRepository<UserLogin, String> { 

}
