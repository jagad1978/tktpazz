package com.tktpass.userportal.repositories;

import org.springframework.data.repository.CrudRepository;

import com.tktpass.userportal.model.Address;

public interface AddressRepository extends CrudRepository<Address, String> { 

}
