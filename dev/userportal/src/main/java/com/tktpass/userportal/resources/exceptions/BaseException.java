package com.tktpass.userportal.resources.exceptions;

import org.springframework.http.HttpStatus;

public class BaseException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ErrMessage errMessage;
	
	private HttpStatus status;
	
	public BaseException(ErrMessage message) {
		this(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	protected BaseException(ErrMessage message, HttpStatus status) {
		super(message.getMessage());
		this.errMessage = message;
		this.status = status;
	}

	public ErrMessage getErrMessage() {
		return errMessage;
	}

	public HttpStatus getStatus() {
		return status;
	}

}
